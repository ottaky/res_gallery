async function doEverything() {

  let galleryItems = [],
      expandos = document.querySelectorAll('[data-url]'),
      matches

  for (let x = 0; x < expandos.length; x++) {

    let thisUrl = expandos[x].getAttribute('data-url')

    // skip relative links
    if (thisUrl.match(/^\//)) continue

    // remove querystrings
    thisUrl = thisUrl.replace(/\?.+$/, '')
    // force https
    thisUrl = thisUrl.replace(/^http:/, 'https:')

    if (thisUrl.match(/\.(jpe?g|gif|png|webm|mp4)$/i)) {
      // direct links
      galleryItems.push(thisUrl)
    }
    else if (thisUrl.match(/\/imgur\.com\/[a-z0-9]+$/i)) {
      // imgur directory => direct link
      let newUrl = thisUrl.replace('imgur', 'i.imgur')
      galleryItems.push(newUrl + '.jpg')
    }
    else if (thisUrl.match(/imgur\.com\/a\//)) {
      // imgur album - using RES's Client-ID (sorry!)
      if (matches = thisUrl.match(/imgur\.com\/a\/([a-z0-9]+)$/i)) {
        let response = await fetch(`https://api.imgur.com/3/album/${matches[1]}/images`, {
          headers: {
            'Authorization': 'Client-ID 1d8d9b36339e0e2'
          }
        })
        if (response.ok) {
          let json = await response.json()
          if ('data' in json && json.data.length) galleryItems = galleryItems.concat(json.data.map(img => { return img.link }))
        }
        else {
          console.log('res_gallery imgur album fetch failed')
        }
      }
    }
    else if (thisUrl.match(/gfycat/)) {
      if (thisUrl.match(/[A-Z]/)) {
        // gfycat with usable URL
        let newUrl = thisUrl.replace(/\/gfycat/, '/giant.gfycat')
        galleryItems.push(newUrl + '.webm')
      }
      else {
        // gfycat with all lower case ID
        if (matches = thisUrl.match(/\gfycat\.com\/(.+)$/)) {
          let response = await fetch(`https://gfycat.com/cajax/get/${matches[1]}`, {
            mode: 'no-cors'
          })
          if (response.ok) {
            let json = await response.json()
            if (('gfyItem' in json) && json.gfyItem.webmUrl) galleryItems.push(json.gfyItem.webmUrl)
          }
          else {
            console.log('res_gallery gfycat data fetch failed')
          }
        }
      }
    }
    else if (thisUrl.match(/vidble\.com\/album/)) {
      // vidble albums
      let response = await fetch(thisUrl + '?json=1')
      if (response.ok) {
        let json = await response.json()
        galleryItems = galleryItems.concat(json.pics.map(img => { return 'https:' + img }))
      }
      else {
        console.log('res_gallery vidble data fetch failed')
      }
    }
    else if (thisUrl.match(/\.gifv$/)) {
      // gifvs to mp4s
      galleryItems.push(thisUrl.replace(/gifv$/, 'mp4'))
    }
    else {
      if (thisUrl.match(/(erome)/)) {
        // TODO! add support for these
      }
      else {
        console.log('res_gallery unsupported url ', thisUrl)
      }
    }
  }

  if (galleryItems.length) {

    let style = document.createElement('style')
    style.innerHTML = `
     .galleryPhotos {
        line-height: 0;
        column-count: auto;
        column-width: 250px;
      }
      .galleryPhotos img, video {
        width: 250px;
      }
    `
    document.head.appendChild(style)

    // force HTTPS URLsthat we might have got from galleries
    galleryItems = galleryItems.map(url => { return url.replace(/^http:/, 'https:') })

    // remove duplicate URLs
    galleryItems = [...new Set(galleryItems)]

    let galleryDiv = document.createElement('div')
    galleryDiv.classList.add('galleryPhotos')

    galleryItems.forEach(itemUrl => {
      galleryDiv.appendChild(getItem(itemUrl))
    })

    document.getElementById('siteTable').insertBefore(galleryDiv, document.getElementById('siteTable').children[0])
    document.getElementsByClassName('res-show-images')[0].children[0].click()
  }
  else {
    console.log('res_gallery found no URLs')
  }
}

function getItem(itemUrl) {
    let a = document.createElement('a')
    a.setAttribute('href', itemUrl)
    a.setAttribute('target', '_blank')
    if (itemUrl.match(/\.(webm|mp4)$/)) {
      let video = document.createElement('video')
      video.setAttribute('title', itemUrl)
      video.setAttribute('src', itemUrl)
      video.setAttribute('autoplay', '1')
      video.setAttribute('loop', '1')
      video.setAttribute('muted', '1')
      a.appendChild(video)
    }
    else {
      let img = document.createElement('img')
      img.setAttribute('title', itemUrl)
      // use medium size imgurs in grid to reduce bandwidth + speed up download
      if (itemUrl.match(/imgur/)) itemUrl = itemUrl.replace(/\.(jpe?g|gif|png)$/, 'm.$1')
      img.setAttribute('src', itemUrl)
      a.appendChild(img)
    }
    return a
}

doEverything()

